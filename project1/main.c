/*  main.c  - main */

#include <xinu.h>

#define BUFFER_SIZE 15
#define RUN_LIMIT 15
#define EXIT_THRESHOLD 100

int buffer[BUFFER_SIZE];


void producer(sid32 producerSemaphore, sid32 consumerSemaphore) {
	int countValue = 0;
	buffer[0] = 0;
	wait(producerSemaphore);
	kprintf("producer starts running\n");
	while (1) {
		if (countValue % RUN_LIMIT == 0 && countValue != 0) {
			kprintf("%s\n", "signal consumer and producer stops running");
			signal(consumerSemaphore);
			wait(producerSemaphore);
			kprintf("%s\n", "producer resumes");
			if (countValue > EXIT_THRESHOLD) break;
		}
		buffer[countValue % BUFFER_SIZE] = countValue;
		kprintf("producer buffer[%d] = %d\n",countValue % BUFFER_SIZE, countValue); 
		countValue++;
	}
}

void consumer( sid32 producerSemaphore, sid32 consumerSemaphore) {
	int32 loopCount = 0;
	wait(consumerSemaphore);
	kprintf("%s\n", "consumer starts running");
	while (1) {
		if (loopCount % RUN_LIMIT == 0 && loopCount != 0) {
			kprintf("%s\n", "signal producer and consumer stops running");
			signal(producerSemaphore);	
			wait(consumerSemaphore);
			kprintf("%s\n", "consumer resumes");
			if (loopCount > EXIT_THRESHOLD) break;
		}
		kprintf("consume %d\n", buffer[loopCount % BUFFER_SIZE]);
		loopCount++;
	}
}
process main(void){
	uint32 stackSize = 1024;
	pri16 processPriority = 20;
	sid32 produced, consumed;
	produced = semcreate(1);
	consumed = semcreate(0);
	resume(create(consumer, stackSize, processPriority, "consumer func in debugging", 2, produced, consumed ));
	resume(create(producer, stackSize, processPriority, "producer func in debugging", 2, produced, consumed ));
}
/*process resumeIntoShell(void)
{
	recvclr();
	resume(create(shell, 8192, 50, "shell", 1, CONSOLE));


	while (TRUE) {
		receive();
		sleepms(200);
		kprintf("\n\nMain process recreating shell\n\n");
		resume(create(shell, 4096, 20, "shell", 1, CONSOLE));
	}
	return OK;
    
}*/
